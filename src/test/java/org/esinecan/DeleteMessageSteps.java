package org.esinecan;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@ContextConfiguration(classes = DiscussionAppApplication.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class DeleteMessageSteps {

    @Given("^I have an existing message with an Id$")
    public void i_have_an_existing_message_with_an_Id() throws Throwable {
    }

    @When("^I call deleteMessage endpoint this time with post method and message's Id$")
    public void i_call_deleteMessage_endpoint_this_time_with_post_method_and_message_s_Id() throws Throwable {
    }

    @Then("^the returned list should not contain the message$")
    public void the_returned_list_should_not_contain_the_message() throws Throwable {
    }

    @When("^I call deleteMessage endpoint this time with the same messageID$")
    public void i_call_deleteMessage_endpoint_this_time_with_the_same_messageID() throws Throwable {
    }

    @Then("^I should receive an error for a message not found$")
    public void i_should_receive_an_error_for_a_message_not_found() throws Throwable {
    }
}
