package org.esinecan;

import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by eren.sinecan on 11.05.17.
 */
//@SpringBootTest
//@RunWith(Cucumber.class)
public class BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);

    @BeforeClass
    public static void runApp(){
        try {
            String[] args = {};
            DiscussionAppApplication.main(args);
            logger.info( "Application started");
        } catch (Exception e) {
            logger.info("Application failed to start. Try running mvn package. " +
                    "Make sure you have internet connection. Here is what went wrong: {}", e);
        }
    }

    @AfterClass
    public static void shutDownApp(){
        try {
            DiscussionAppApplication.shutDown();
            logger.info( "Application stopped");
        } catch (Exception e) {
            logger.info( "Application failed to stop. Port 8088 might be hogged. " +
                    "Try running \"lsof -n -i4TCP:8088 | grep LISTEN\" to see if that's the case. " +
                    "Here is what went wrong: {}", e);
        }
    }
}
