package org.esinecan;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@ContextConfiguration(classes = DiscussionAppApplication.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class UpdatePostSteps {
    @Given("^I create a Message Object with an existing messageId and message saying hello world$")
    public void i_create_a_Message_Object_with_an_existing_messageId_and_message_saying_hello_world() throws Throwable {

    }

    @When("^I call updateMessage endpoint this time with post method$")
    public void i_call_updateMessage_endpoint_this_time_with_post_method() throws Throwable {

    }

    @Then("^the returned list should contain the message that had lorem ipsum before, but with a hello world message$")
    public void the_returned_list_should_contain_the_message_that_had_lorem_ipsum_before_but_with_a_hello_world_message() throws Throwable {

    }

    @When("^I call updateMessage endpoint this time with a nonexistent messageID$")
    public void i_call_updateMessage_endpoint_this_time_with_a_nonexistent_messageID() throws Throwable {

    }

    @Then("^I should receive an error$")
    public void i_should_receive_an_error() throws Throwable {

    }
}
