package org.esinecan;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/updateMessage.feature")
public class UpdateMessageTest extends BaseTest {
}
