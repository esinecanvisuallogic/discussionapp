package org.esinecan;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.entity.Message;
import org.esinecan.service.contract.MessageService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@ContextConfiguration(classes = DiscussionAppApplication.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class CreateMessageSteps {

    @Autowired
    private MessageService service;

    private Message message;

    private Message childMessage;

    private List<Message> allMessages;

    @Given("^I send a put request to sendMessage endpoint with username as createTest and message a lorem ipsum$")
    public void i_send_a_put_request_to_sendMessage_endpoint_with_username_as_createTest_and_message_a_lorem_ipsum() throws Throwable {
        message = new Message();
        message.setUsername("createTest");
        message.setContent("lorem ipsum");
    }

    @When("^I call messages endpoint with get method$")
    public void i_call_messages_endpoint_with_get_method() throws Throwable {
        allMessages = service.createMessage(message);
    }

    @Then("^the returned list should contain my message$")
    public void the_returned_list_should_contain_my_message() throws Throwable {
        assertEquals(allMessages.size(), 1);
        assertEquals(allMessages.get(0).getContent(), "lorem ipsum");
    }

    @Then("^My message should have a child number of zero/$")
    public void my_message_should_have_a_child_number_of_zero() throws Throwable {
        assertEquals(allMessages.get(0).getChildren().size(), 0);
    }

    @When("^I call messages endpoint with get method with username as createTest, message as hi and parent as newly created message$")
    public void i_call_messages_endpoint_with_get_method_with_username_as_createTest_message_as_hi_and_parent_as_newly_created_post() throws Throwable {
        childMessage = new Message();
        childMessage.setUsername("createTest");
        childMessage.setContent("lorem ipsum");
        childMessage.setParent(allMessages.get(0));
        allMessages = service.createMessage(message);
    }

    @Then("^the returned list should contain my message which contains child message$")
    public void the_returned_list_should_contain_my_message_which_contains_child_message() throws Throwable {
        assertEquals(allMessages.size(), 1);
    }

    @Then("^My message should have one as the child number$")
    public void my_message_should_have_one_as_the_child_number() throws Throwable {
        assertEquals(allMessages.get(0).getChildren().size(), 1);
    }
}
