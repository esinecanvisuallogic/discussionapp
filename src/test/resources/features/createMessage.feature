Feature: create a message

Narrative:
As a user
I want to create a message

Scenario: creating a new message
Given I send a put request to sendMessage endpoint with username as createTest and message a lorem ipsum
When I call messages endpoint with get method
Then the returned list should contain my message
  And My message should have a child number of zero/
When I call messages endpoint with get method with username as createTest, message as hi and parent as newly created message
Then the returned list should contain my message which contains child message
  And My message should have one as the child number
