Feature: Update a message

Narrative:
As a user
I want to update a message

Scenario: updating a message
Given I create a Message Object with an existing messageId and message saying hello world
When I call updateMessage endpoint this time with post method
Then the returned list should contain the message that had lorem ipsum before, but with a hello world message
When I call updateMessage endpoint this time with a nonexistent messageID
Then I should receive an error