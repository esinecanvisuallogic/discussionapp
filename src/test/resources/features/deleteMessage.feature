Feature: delete a post

Narrative:
As a user
I want to delete a post

Scenario: deleting a post
Given I have an existing message with an Id
When I call deleteMessage endpoint this time with post method and message's Id
Then the returned list should not contain the message
When I call deleteMessage endpoint this time with the same messageID
Then I should receive an error for a message not found