angular.module('discussion', ['ui.bootstrap']).controller('helloworld',
    function($http) {
        var self = this;
        self.testMsg = "Hello World";
        self.error = false;
        self.errMsg = '';
        self.success = false;
        self.succMsg = '';

        self.callTest = function () {
            alert(self.testMsg);
        }

        self.init = function () {
            $http.get("/messages").then(function (response) {
                self.allMessages = response.data;
            }, function (error) {
                self.error = true;
                self.errMsg = error.data.message;
            })
        }
        self.init();

        /**
         * This function is called when we receive an error. It displays the error on top for 3 seconds.
         *
         * @param errMsg The error message from the server side controller.
         */
        self.displayError = function (errMsg) {

            self.showErrorMsg = true;
            self.errMsg = errMsg;

            $timeout(function () {
                self.showErrorMsg = false;
                self.errorMsg = "";
            }, 3000);

        };

        /**
         * This function is called when we do a successful transaction. It displays the message on top for 3 seconds.
         *
         * @param succMsg The success message from the client side controller. (self)
         */
        self.displaySuccess = function (succMsg) {

            self.showSuccessMsg = true;
            self.successMsg = succMsg;

            $timeout(function () {
                self.showSuccessMsg = false;
                self.successMsg = "";
            }, 3000);

        };
    });