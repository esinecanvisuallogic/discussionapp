package org.esinecan.persistence;

import org.esinecan.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
}
