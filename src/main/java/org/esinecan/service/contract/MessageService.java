package org.esinecan.service.contract;

import javassist.NotFoundException;
import org.esinecan.entity.Message;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

/**
 * Created by eren.sinecan on 11.05.17.
 */
public interface MessageService {

    List<Message> readMessages();

    List<Message> updateMessage(Message message) throws ChangeSetPersister.NotFoundException, NotFoundException;

    List<Message> createMessage(Message message);

    List<Message> deleteMessage(Long messageId) throws NotFoundException;
}
