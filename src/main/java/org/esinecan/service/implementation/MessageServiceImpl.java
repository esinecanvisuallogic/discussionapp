package org.esinecan.service.implementation;

import javassist.NotFoundException;
import org.esinecan.entity.Message;
import org.esinecan.persistence.MessageRepository;
import org.esinecan.service.contract.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.Objects.isNull;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository repository;


    @Override
    @Transactional(readOnly = true)
    public List<Message> readMessages() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public List<Message> updateMessage(Message message) throws NotFoundException {
        if(repository.exists(message.getId())){
            repository.saveAndFlush(message);
            return readMessages();
        }
        throw new NotFoundException("The post you're trying to update is not found: " + message.toString());
    }

    @Override
    @Transactional
    public List<Message> createMessage(Message message) {
        if(!isNull(message.getId())){
            message.setId(null);
        }
        repository.saveAndFlush(message);
        return readMessages();
    }

    @Override
    @Transactional
    public List<Message> deleteMessage(Long messageId) throws NotFoundException {
        if(repository.exists(messageId)){
            repository.delete(messageId);
            return readMessages();
        }
        throw new NotFoundException("The post you're trying to update is not found: " + messageId);
    }
}
