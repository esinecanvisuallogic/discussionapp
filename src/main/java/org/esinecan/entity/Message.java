package org.esinecan.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@Data
public class Message {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String content;

    private String username;

    private Message parent;

    @OneToMany(mappedBy="parent")
    private List<Message> children;
}
