package org.esinecan.web;

import org.esinecan.entity.Message;
import org.esinecan.service.contract.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by eren.sinecan on 11.05.17.
 */
@RestController
public class DiscussionController {

    @Autowired
    private MessageService service;

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public List<Message> getMessages() {
        return service.readMessages();
    }

    @RequestMapping(value = "/create-message", method = RequestMethod.PUT)
    public List<Message> createMessage(@RequestBody Message message){
        return service.createMessage(message);
    }

    @RequestMapping(value = "/update-message", method = RequestMethod.POST)
    public List<Message> updateMessage(@RequestBody Message message) throws Exception {
        return service.updateMessage(message);
    }

    @RequestMapping(value = "/delete-message", method = RequestMethod.POST)
    public List<Message> deleteMessage(@RequestBody Long messageId) throws Exception {
        return service.deleteMessage(messageId);
    }
}
